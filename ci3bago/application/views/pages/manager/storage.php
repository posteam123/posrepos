      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
			  <!-- Trigger the modal with a button -->
			  <br><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"><i class="fa fa-barcode fa-fw"></i>&nbsp;Product Transfer</button>
			  <!-- Modal -->
			<div class="modal fade" id="myModal" role="dialog">
				<div class="modal-dialog">
				
				  <!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Product Details</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label>Barcode:</label>
									<input type="text" class="form-control">
							</div>
							<div class="form-group">
								<label for="sel1">Category:</label>
									<select class="form-control" id="sel1">
										<option>1</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
									</select>
							</div>
							  <div class="form-group">
								<label>Brand:</label>
									<input type="text" class="form-control">
							  </div>
								<div class="form-group">
								<label>Description:</label>
									<input type="text" class="form-control">
							  </div>
								<div class="form-group">
								<label>Qty.:</label>
									<input type="text" class="form-control">
							  </div>
								<div class="form-group">
								<label>Price:</label>
									<input type="text" class="form-control">
							  </div>
						</div>
						<div class="modal-footer">
						  <button type="button" class="btn btn-default" data-dismiss="modal">Save</button>
						  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<!--END MODALS-->
			<div class="row mt">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading" style="background: #1e90ff; color:white;">
						  <h4><strong>Storage</strong></h4>
						</div>
						<div class="panel-body here">
							<div class="dataTable_wrapper">
								<table class="table table-bordered table-striped table-condensed" id="dataTables-example">
								  <thead>
								  <tr>
									  <th>Barcode</th>
									  <th>Category</th>
									  <th>Brand</th>
									  <th>Description</th>
									  <th class="numeric">Qty.</th>
									  <th class="numeric">Price</th>
								  </tr>
								  </thead>
								  <tbody>
								  <tr>
									  <th>1234567890987654321</th>
									  <th>Beverages</th>
									  <th>Pocari Sweat</th>
									  <th>1L</th>
									  <th>200</th>
									  <th>35.00</th>
								  </tr>
								  <tr>
									  <th>1234567890987654321</th>
									  <th>Beverages</th>
									  <th>Pocari Sweat</th>
									  <th>1L</th>
									  <th>200</th>
									  <th>35.00</th>
								  </tr>
								  <tr>
									  <th>1234567890987654321</th>
									  <th>Beverages</th>
									  <th>Pocari Sweat</th>
									  <th>1L</th>
									  <th>200</th>
									  <th>35.00</th>
								  </tr>
								  <tr>
									  <th>1234567890987654321</th>
									  <th>Beverages</th>
									  <th>Pocari Sweat</th>
									  <th>1L</th>
									  <th>200</th>
									  <th>35.00</th>

								  </tr>
								  <tr>
									  <th>1234567890987654321</th>
									  <th>Beverages</th>
									  <th>Pocari Sweat</th>
									  <th>1L</th>
									  <th>200</th>
									  <th>35.00</th>
								  </tr>
								  <tr>
									  <th>1234567890987654321</th>
									  <th>Beverages</th>
									  <th>Pocari Sweat</th>
									  <th>1L</th>
									  <th>200</th>
									  <th>35.00</th>
								  </tr>
								  </tbody>
							  </table>
						  </div>
						</div>
					</div><!-- /content-panel -->
				</div><!-- /col-lg-4 -->			
			</div><!-- /row -->
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->
      <!--main content end-->
  </section>

