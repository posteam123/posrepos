      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start--><div class="showtime"></div>
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="<?php echo base_url('manager/profile'); ?>"><img src="<?php echo base_url('assets/img/logo.png'); ?>" class="img-circle" width="60"></a></p>
              	  <h5 class="centered">WMS POS System</h5>

                  <li>
                      <a href="<?php echo base_url("manager/"); ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                  </li>
                  <li>
                      <a href="<?php echo base_url("manager/store"); ?>"><i class="fa fa-home fa-fw"></i> Stores</a>
                  </li>
                  
                  <li>
                      <a href="<?php echo base_url("manager/pos"); ?>" ><i class="fa fa-desktop"></i> POS</a>
                  </li>
                  
                  <li>
                    <a href="<?php echo base_url("manager/employee"); ?>"><i class="fa fa-user fa-fw"></i> Employee<span class="fa arrow"></span></a>
                  </li>
				  
				  <li>
					<a href="<?php echo base_url("manager/product"); ?>"><i class="fa fa-barcode fa-fw"></i> Products<span class="fa arrow"></span></a>
				  </li>
				  
				  <li>
					<a href="<?php echo base_url("manager/storage"); ?>"><i class="fa fa-bar-chart-o fa-fw"></i> Storage<span class="fa arrow"></span></a>
				  </li>
				  
				  <li class="sub-menu">
					<a href="javascript:;" ><i class="fa fa-truck fa-fw"></i> Supplier<span class="fa arrow"></span></a>
					<ul class="sub">
						<li>
							<a href="<?php echo base_url("manager/transaction"); ?>">Transactions</a>
						</li>
						<li>
							<a href="<?php echo base_url("manager/supplier"); ?>">List of Supplier</a>
						</li>
					</ul>
				  </li>
				  
				  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-file-o fa-fw"></i>
                          <span> Reports</span>
                      </a>
                      <ul class="sub">
                          <li><a href="<?php echo base_url("manager/salesreport"); ?>">Sales Reports</a></li>
                          <li><a href="<?php echo base_url("manager/topselling"); ?>">Top Selling Reports</a></li>
                      </ul>
                  </li>
				  
				  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-gear fa-fw"></i>
                          <span> Settings</span>
                      </a>
                      <ul class="sub">
                          <li><a href="<?php echo base_url("manager/profile"); ?>">Edit Information</a></li>
                      </ul>
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->