
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
			<br />
			<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">
				<i class="fa fa-list-ul fa-fw"></i>&nbsp;Add Category
			</button>
			  <!--MODALS-->
			  <div class="modal fade" id="myModal" role="dialog">
				<form id="registerForm" role="form" name="form" data-toggle="validator" method="post" action="<?php echo base_url('addcategory'); ?>">
				
					<div class="alert alert-danger hidden" id="msg" role="alert">
					  <span class="glyphicon glyphicon-check">
					  </span>
					</div>
					
					<div class="modal-dialog">
					  <!-- Modal content-->
					  <div class="modal-content">
						<div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Category Details</h4>
						</div>
						<div class="modal-body">
						  <div class="form-group has-feedback">
							<label for="cname">Category Name:</label>
							<input type="text" class="form-control" id="cname" name="categoryname" pattern="([a-z]*[A-Z]*[/- ]*)*" data-error="Not a valid input" required>
							<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							<p class="help-block with-errors"></p>
						  </div>
						
						  <div class="form-group has-feedback">
							<label for="description">Description:</label>
							<input type="text" class="form-control" id="desc" name="description" pattern="([a-z]*[A-Z]*[-,/ ]*)*" data-error="Not a valid input" required>
							<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							<p class="help-block with-errors"></p>
						  </div>
						</div>
						<div class="modal-footer">
						  <button type="submit" id="signup" class="btn btn-default">Submit</button>
						  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					  </div>
					</div>
				</form>
			  </div>
			<!--END MODALS-->
			<div class="row mt">
				<div class="col-lg-12">
                    <div class="row mtbox">
						<?php foreach($category as $cat): ?>
							<a href="">
								<div class="col-md-3 col-sm-3 box0">
									<div class="box1">
										<img class="category-img" src="<?php echo base_url('assets/img/category/'.$cat->c_id.'.png'); ?>" alt="" />
										<h3><?php echo $cat->name; ?></h3>
										<small><?php echo $cat->description; ?></small>
									</div>
								</div>
							</a>
						<?php endforeach; ?>
                  	</div>
                </div><!-- /col-lg-4 -->				
		  	</div><!-- /row -->
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->
      <!--main content end-->
  </section>

