
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
     <section id="main-content">
          <section class="wrapper">
			<br />
			<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal2">
				<i class="fa fa-list-ul fa-fw"></i>&nbsp;Add Product
			</button>

			<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">
				<i class="fa fa-list-ul fa-fw"></i>&nbsp;Add Category
			</button>
			  <!--MODALS-->

 <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Product Details</h4><br>
        </div>
        <div class="modal-body">
        <form role="form" data-toggle="validator">
         <div class="form-group"><!--This auto generated-->
            <label>Barcode:</label>
                <input type="text" class="form-control">
          </div>
          <div class="form-group">
            <label>Product Type:</label>
                <input type="text" class="form-control">
          </div>
        
          <div class="form-group">
            <label>Product Brand:</label>
                <input type="text" class="form-control">
          </div>
           <div class="form-group">
            <label>Product Category:</label><!--This is also auto generated field based on category table-->
                <input type="text" class="form-control">
          </div>
          <div class="form-group">
            <label>Quantity:</label><!--This is also auto generated field based on category table-->
                <input type="text" class="form-control">
          </div>
          <div class="form-group">
            <label>Price:</label><!--This is also auto generated field based on category table-->
                <input type="text" class="form-control">
          </div>
           <div class="form-group">
            <label>Damage:</label><!--This is also auto generated field based on category table-->
                <input type="text" class="form-control">
          </div>
		  
		  <div class="form-group">
									<label for="dmage">Product Details:</label>
									<input type="text" class="form-control" id="dmage" name="damage">
								</div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
</div>
</div>

			<div class="modal fade" id="myModal" role="dialog">
				<div class="modal-dialog">
				
				  <!-- Modal content-->
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">Category Details</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label>Category Name:</label>
							<input type="text" name="name" class="form-control">
						</div>
						<!--<div class="form-group">
							<label>Quantity:</label>
							<input type="text" class="form-control">
						</div>
						<div class="form-group">
							<label>Description:</label>
							<input type="text" class="form-control">
						</div>-->
					</div>
					<div class="modal-footer">
					  <button type="button" class="btn btn-default" data-dismiss="modal">Save</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				  </div>
				</div>
			</div>

			<!--END MODALS-->
			<!--Background Design-->
			<div class="row mt">
				<div class="col-lg-12">
                    <div class="row mtbox">
						<?php foreach($category as $cat): ?>
							<a href="">
								<div class="col-md-3 col-sm-3 box0">
									<div class="box1">
										<img class="category-img" src="<?php echo base_url('assets/img/category/'.$cat->c_id.'.png'); ?>" alt="" />
										<h3><?php echo $cat->name; ?></h3>
										<small><?php echo $cat->description; ?></small>
									</div>
								</div>
							</a>
						<?php endforeach; ?>
                  	</div>
                </div><!-- /col-lg-4 -->				
		  	</div><!-- /row -->
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->
      <!--main content end-->
  </section>

