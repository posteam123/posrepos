      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
		<section class="wrapper">
			<!-- Trigger the modal with a button -->
			<br><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"><i class="fa fa-user fa-fw"></i>&nbsp;Add Employee</button>
			
			<!--MODALS-->
			<div class="modal fade" id="myModal" role="dialog">
		  
				<div class="modal-dialog">]
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Employee Details</h4>
						</div>
						<div class="modal-body">
							<form role="form">
								<div class="form-group">
									<label for="empid">Employee ID:</label>
									<input type="text" class="form-control" id="empid">
								</div>
								<div class="form-group">
									<label for="uname">Username:</label>
									<input type="text" class="form-control" id="uname">
								</div>
								<div class="form-group">
									<label for="fname">First Name:</label>
									<input type="text" class="form-control" id="fname">
								</div>
								<div class="form-group">
									<label for="mname">Middle Name:</label>
									<input type="text" class="form-control" id="mname">
								</div>
								<div class="form-group">
									<label for="lname">Last Name:</label>
									<input type="text" class="form-control" id="lname">
								</div>
								<div class="form-group">
									<label for="address">Address:</label>
									<input type="text" class="form-control" id="address">
								</div>
								<div class="form-group">
									<label for="contactno">Contacnt No.:</label>
									<input type="text" class="form-control" id="contactno">
								</div>
								<div class="form-group">
									<label for="sel1">Position:</label>
										<select class="form-control" id="sel1">
											<option>Manager</option>
											<option>Supervisor</option>
											<option>Checker</option>
											<option>Chashier</option>
										</select>
								</div>
					
							</form>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-default">Submit</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</div>
				</div>
			</div>
		  <!--END MODALS-->
			<div class="row mt">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading" style="background: #1e90ff; color:white;">
						  <h4><strong>List of Employees</strong></h4>
						</div>
						<div class="panel-body here">
							<div class="dataTable_wrapper">
								<table class="table table-bordered table-striped table-condensed" id="dataTables-example">
									<thead>
									<tr>
										<th class="numeric">User ID</th>
										<th class="numeric">Employee ID</th>
										<th>Username</th>
										<th>First Name</th>
										<th>Middle Name</th>
										<th>Last Name</th>
										<th>Address</th>
										<th class="numeric">Contact No.</th>
									</tr>
									</thead>
									<tbody>
										<tr>
											<td>1234</td>
											<td class="numeric">12345678</td>
											<td class="numeric">qwerty</td>
											<td>Web</td>
											<td>Media</td>
											<td>Solutions</td>
											<td>Tiano-Fernandez St.,Brgy 1, Cagayan de Oro City</td>
											<td class="numeric">088 881 0269</td>
										</tr>
										<tr>
											<td>1234</td>
											<td class="numeric">12345678</td>
											<td class="numeric">qwerty</td>
											<td>Web</td>
											<td>Media</td>
											<td>Solutions</td>
											<td>Tiano-Fernandez St.,Brgy 1, Cagayan de Oro City</td>
											<td class="numeric">088 881 0269</td>
										</tr>
										<tr>
											<td>1234</td>
											<td class="numeric">12345678</td>
											<td class="numeric">qwerty</td>
											<td>Web</td>
											<td>Media</td>
											<td>Solutions</td>
											<td>Tiano-Fernandez St.,Brgy 1, Cagayan de Oro City</td>
											<td class="numeric">088 881 0269</td>
										</tr>
										<tr>
											<td>1234</td>
											<td class="numeric">12345678</td>
											<td class="numeric">qwerty</td>
											<td>Web</td>
											<td>Media</td>
											<td>Solutions</td>
											<td>Tiano-Fernandez St.,Brgy 1, Cagayan de Oro City</td>
											<td class="numeric">088 881 0269</td>
										</tr>
										<tr>
											<td>1234</td>
											<td class="numeric">12345678</td>
											<td class="numeric">qwerty</td>
											<td>Web</td>
											<td>Media</td>
											<td>Solutions</td>
											<td>Tiano-Fernandez St.,Brgy 1, Cagayan de Oro City</td>
											<td class="numeric">088 881 0269</td>
										</tr>
										<tr>
											<td>1234</td>
											<td class="numeric">12345678</td>
											<td class="numeric">qwerty</td>
											<td>Web</td>
											<td>Media</td>
											<td>Solutions</td>
											<td>Tiano-Fernandez St.,Brgy 1, Cagayan de Oro City</td>
											<td class="numeric">088 881 0269</td>
										</tr>
									</tbody>
							  </table>
							</div>
						</div><!-- /content-panel -->
					</div><!-- /col-lg-4 -->			
				</div>
		  	</div><!-- /row -->
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->
      <!--main content end-->
  </section>
