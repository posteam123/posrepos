      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
		      <!-- Trigger the modal with a button -->
		      <br><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"><i class="fa fa-home fa-fw"></i>&nbsp;Add Branch</button><br>
				<!--MODALS-->
			<div class="modal fade" id="myModal" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Store Details</h4>
						</div>
							<div class="modal-body">
							<form role="form">
								<div class="form-group">
									<label for="branchname">Branch Name:</label>
									<input type="text" class="form-control" id="branchname">
								</div>
								<div class="form-group">
									<label for="address">Address:</label>
									<input type="text" class="form-control" id="address">
								</div>
								<div class="form-group">
									<label for="supervisor">Branch Supervisor:</label>
									<input type="text" class="form-control" id="supervisor">
								</div>
								<div class="form-group">
									<label for="contactno">Contact No.:</label>
									<input type="text" class="form-control" id="contactno">
								</div>
							</form>
							</div>
							<div class="modal-footer">
								<button type="submit" class="btn btn-default">Submit</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							</div>
					</div>
				</div>
			</div>
			<!--END MODALS-->
            <br>
            <!-- DATATABLES -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
						<div class="panel-heading" style="background: #1e90ff; color:white;">
								  <h4><strong>List of Stores</strong></h4>
						</div>
						<div class="panel-body here">
							<div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Branch Name</th>
                                            <th>Address</th>
                                            <th>Branch Supervisor</th>
                                            <th>Contact no.</th>
                                            <th>Action</th>
                                            <th>Sample</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>GAISANO</td>
                                            <td>Don Apolinar Velez St., Cagayan de Oro City</td>
                                            <td>Ms. Jireh Felizardo</td>
                                            <td>(088)586-9906</td>
                                         
                                        </tr>
                                        <tr>
                                            <td>ORORAMA</td>
                                            <td>Don Apolinar Velez St., Cagayan de Oro City</td>
                                            <td>Ms. Jireh Felizardo</td>
                                            <td>(088)586-9906</td>
                                         
                                        </tr>
                                        <tr>
                                            <td>AYALA</td>
                                            <td>Don Apolinar Velez St., Cagayan de Oro City</td>
                                            <td>Ms. Jireh Felizardo</td>
                                            <td>(088)586-9906</td>
                                      
                                        </tr>
                                        <tr>
                                            <td>SM</td>
                                            <td>Don Apolinar Velez St., Cagayan de Oro City</td>
                                            <td>Ms. Jireh Felizardo</td>
                                            <td>(088)586-9906</td>
                                  
                                        </tr>
                                        <tr>
                                            <td>KETKAI</td>
                                            <td>Don Apolinar Velez St., Cagayan de Oro City</td>
                                            <td>Ms. Jireh Felizardo</td>
                                            <td>(088)586-9906</td>
                                    
                                        </tr>
                                        <tr>
                                            <td>DUTYFREE</td>
                                            <td>Don Apolinar Velez St., Cagayan de Oro City</td>
                                            <td>Ms. Jireh Felizardo</td>
                                            <td>(088)586-9906</td>
                                          
                                        </tr>
                                        <tr>
                                            <td>PUREGOLD</td>
                                            <td>Don Apolinar Velez St., Cagayan de Oro City</td>
                                            <td>Ms. Jireh Felizardo</td>
                                            <td>(088)586-9906</td>
                                
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->
      <!--main content end-->
  </section>

    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

