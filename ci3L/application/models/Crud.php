<?php
class Crud extends CI_Model {
	public function __construct()
	{
			parent::__construct();
			
	}
	
	public function addemployee( $data)
	{
		$this->db->insert('users', $data);
		return;
	}
	
	public function addcategory( $data)
	{
		$this->db->insert('category', $data);
		return;
	}
	
	public function get_any($tablename,$except)
	{
		if ($except == " ")
		{
			$query = $this->db->get($tablename);
			return $query->result();
		}
		else
		{
			$this->db->select("*");
			$this->db->from("users a, role b");
			$this->db->where("a.userlevel = b. userlevel");
			$this->db->where("a.userlevel != '$except'");
			$query = $this->db->get();
			return $query->result();
		}
	}
	
	public function get($tablename)
	{
		$query = $this->db->get($tablename);
		return $query->result();
	}
	
	public function changestat($data,$id)
	{
		$this->db->where('u_id',$id);
		$this->db->update('users',$data);
	}
	
	public function logindetails($username)
	{
		$this->db->where('username',$username);
		$query = $this->db->get('users');
		return $query->row();
	}
	
	public function userdetails($id)
	{
		$this->db->where('u_id',$id);
		$query = $this->db->get('users');
		return $query->row();
	}
	
	public function userlevel($id){
		$this->db->select('a.* , b.userlevel');
		$this->db->from('users a,role as b ');
		$this->db->where('u_id',$id);
		$this->db->where("a.ul_id=b.ul_id");
		$query=$this->db->get();
		return $query->row();
	}

	public function get_storage($tablename,$except)
  {
    if ($except == " ")
    {
      $query = $this->db->get($tablename);
      return $query->result();
    }
    else
    {
      $this->db->select("*");
      $this->db->from("storage a, category b");
      $this->db->where("a.c_id = b. c_id");
      $this->db->where("a.c_id != $except");
      $query = $this->db->get();
      return $query->result();
    }
   }
    
    public function prodTransfer( $data)
	{
		$this->db->insert('storage', $data);
		return;
	}
  
}?>