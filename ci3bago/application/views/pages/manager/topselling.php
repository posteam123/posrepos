<section id="main-content">
	<section class="wrapper">
	<!DOCTYPE html>
	<html>
	<head>
		<link rel="stylesheet" type="text/css" href="../assets/css/main.css">
		<meta charset="UTF-8">
	    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="main.css">
		<script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
		<script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js"></script>
		<script src="jchart.js"></script>
	
	<script>
		$(document).ready(function() {
			$("#population_chart").jChart({x_label:"S A L E S"});
			$("#colors_chart").jChart();
		});
	</script>
	</head>
	<style>
		.jumbotron {
			padding-top: 20px;
			padding-bottom: 10px;
			color: white;
			background-color: #4570a5;
		}
		.jumbotron>h1 {
			font-size: 75pt;
			font-family: "Times New Roman", Times, serif;
			margin: 0;
		}
		.jumbotron>p {
			margin: 0;

		}
	</style>
	
	<body>
	<div class="container">
	<div id="population_chart" data-sort="false" data-width="800" class="jChart chart-lg" name="TOP SELLING PRODUCT<br>">
	<div class="define-chart-row" data-color="#84d6ff" title="Coffee">350000</div>
	<div class="define-chart-row" data-color="red" title="Beer">300500</div>
	<div class="define-chart-row" data-color="gray" title="Biscuit">450000</div>
	<div class="define-chart-row" data-color="#c9aa5f" title="Noodles">250000</div>
	<div class="define-chart-row" data-color="#0074AA" title="Coke">466000</div>
	<div class="define-chart-row" data-color="#adff2f" title="cake">326000</div>
	<div class="define-chart-footer">100000</div>
	<div class="define-chart-footer">200000</div>
	<div class="define-chart-footer">300000</div>
	<div class="define-chart-footer">400000</div>
	<div class="define-chart-footer">500000</div>
	</div>
</div>
</body>
</html>
</section>
</section>