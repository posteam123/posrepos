<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function manager($page = 'index')
	{
		$data['userlevel']=$this->Crud->get_any('role',$except=' ');
		$data['users']=$this->Crud->get_any('users',$except='Admin');
		$data['category']=$this->Crud->get('category',$except=' ');
		$data['company']=$this->Crud->get('company',$except=' ');
		
		$ses=$this->session->userdata('logged_in'); 
		$id=$ses['u_id'];
		$user = $this->Crud->userdetails($id);
		$data['lastname'] = $user->lastname;
		$data['firstname'] = $user->firstname;
		
		$data['role']='manager';
		

		$this->load->view('templates/header',$data);
		$this->load->view('templates/role',$data);
		$this->load->view('templates/sidebar');
		$this->load->view('pages/manager/'.$page, $data);
		$this->load->view('templates/footer');
	}
	
	public function empstatus($status,$u_id)
	{
			$this->session->set_flashdata('statusid',$u_id);
			$this->session->set_flashdata('status',$status);
			redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function change_empstatus()
	{
			$id = $this->input->post('id');
			$status =$this->input->post('status');
			
			$data= array(
				'status'=>$status
			);
			
			$this->Crud->changestat($data,$id);
			$this->session->set_flashdata('updated','Officially Updated');
			redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function cashier($page = 'index')
	{
		$data['company']=$this->Crud->get('company',$except=' ');
		
		$ses=$this->session->userdata('logged_in'); 
		$id=$ses['u_id'];
		$user = $this->Crud->userdetails($id);
		$data['lastname'] = $user->lastname;
		$data['firstname'] = $user->firstname;
		
		$data['role']='cashier';
		
		$this->load->view('templates/header_cashier',$data);
		$this->load->view('templates/role',$data);
		$this->load->view('pages/cashier/'.$page, $data);
		$this->load->view('templates/footer');
	}
	
	public function supervisor($page = 'product')
	{
		$data['company']=$this->Crud->get('company',$except=' ');
		$data['category']=$this->Crud->get('category',$except=' ');
		
		$ses=$this->session->userdata('logged_in'); 
		$id=$ses['u_id'];
		$user = $this->Crud->userdetails($id);
		$data['lastname'] = $user->lastname;
		$data['firstname'] = $user->firstname;
		
		$data['role']='supervisor';
		
		$this->load->view('templates/header_supervisor',$data);
		$this->load->view('templates/role',$data);
		$this->load->view('templates/sidebar_supervisor');
		$this->load->view('pages/supervisor/'.$page, $data);
		$this->load->view('templates/footer');
	}
	
	public function addemployee()
	{
		// for future use :) //
		$this->form_validation->set_rules('firstname', 'Firstname', 'trim|required');
		$this->form_validation->set_rules('empid', 'EmployeeID', 'trim|required');
		$this->form_validation->set_rules('lastname', 'Lastname', 'trim|required');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[user.username]');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('gender', 'Gender', 'trim|required');
		$this->form_validation->set_rules('contactno', 'Contact No', 'trim|required|is_unique[user.contactno]|regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|is_unique[user.emailadd]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|matches[password]');
	
	$firstname = $this->input->post('firstname',TRUE);
	$lastname = $this->input->post('lastname',TRUE);
	$id = $this->input->post('id',TRUE);
	$contactno = $this->input->post('contactno',TRUE);
	$address = $this->input->post('address',TRUE);
	$username = $this->input->post('username',TRUE);
	$userlevel = $this->input->post('userlevel',TRUE);
	$mname = $this->input->post('middlename',TRUE);
	$password ='123';
	$hash = password_hash("$password", PASSWORD_DEFAULT);
	$data= array(
			'id' => $id,
			'firstname' => $firstname,
			'lastname' => $lastname,
			'middlename' => $mname,
			'address' => $address,
			'contactno' => $contactno,
			'username' => $username,
			'password' => $hash,
			'userlevel' => lcfirst($userlevel)
		);
			$this->Crud->add('users',$data);
	}
	
	public function addcategory()
	{
		$this->form_validation->set_rules('categoryname', 'Category Name', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'trim|required');
		
		$categoryname = $this->input->post('categoryname',TRUE);
		$description = $this->input->post('description',TRUE);
		$data= array(
			'name' => $categoryname,
			'description' => $description
		);
			$this->Crud->add('category',$data);
			$this->session->set_flashdata('success', 'Category successfully added');
			redirect("manager/product");
	}

}
