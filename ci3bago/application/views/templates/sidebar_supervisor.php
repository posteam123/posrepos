      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start--><div class="showtime"></div>
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="<?php echo base_url('supervisor'); ?>"><img src="<?php echo base_url('assets/img/logo.png'); ?>" class="img-circle" width="60"></a></p>
              	  <h5 class="centered">WMS POS System</h5>
				  
				  <li>
					<a href="<?php echo base_url("supervisor/product"); ?>"><i class="fa fa-barcode fa-fw"></i> Products<span class="fa arrow"></span></a>
				  </li>
				  
				  <li>
					<a href="<?php echo base_url("supervisor/storage"); ?>"><i class="fa fa-bar-chart-o fa-fw"></i> Storage<span class="fa arrow"></span></a>
				  </li>
				  
				  <li class="sub-menu">
					<a href="javascript:;" ><i class="fa fa-truck fa-fw"></i> Supplier<span class="fa arrow"></span></a>
					<ul class="sub">
						<li>
							<a href="<?php echo base_url("supervisor/transaction"); ?>">Transactions</a>
						</li>
						<li>
							<a href="<?php echo base_url("supervisor/supplier"); ?>">List of Supplier</a>
						</li>
					</ul>
				  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->