N      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
		<section class="wrapper">
			<div class="row mt">
				<div class="col-lg-12">
				  <h1 class="page-header">Personal info</h1>
				  <div class="row">
					<!-- left column -->
					<div class="col-md-2 col-sm-6 col-xs-12">
					</div>
					<!-- edit form column -->
					<div class="col-md-8 col-sm-6 col-xs-12 personal-info">
					  <div class="alert alert-info alert-dismissable">
						<a class="panel-close close" data-dismiss="alert">×</a> 
						<i class="fa fa-coffee"></i>
						This is an <strong>.alert</strong>. Use this to show important messages to the user.
					  </div>
					  <form class="form-horizontal" role="form">
						<div class="form-group">
						  <label class="col-lg-3 control-label">First name:</label>
						  <div class="col-lg-8">
							<input class="form-control" type="text">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-lg-3 control-label">Middle name:</label>
						  <div class="col-lg-8">
							<input class="form-control" type="text">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-lg-3 control-label">Last name:</label>
						  <div class="col-lg-8">
							<input class="form-control" type="text">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-lg-3 control-label">Address:</label>
						  <div class="col-lg-8">
							<input class="form-control" type="text">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-lg-3 control-label">Contact Number:</label>
						  <div class="col-lg-8">
							<input class="form-control" type="text">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3 control-label">Username:</label>
						  <div class="col-md-8">
							<input class="form-control" type="text">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3 control-label">Password:</label>
						  <div class="col-md-8">
							<input class="form-control" type="password">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3 control-label">Confirm password:</label>
						  <div class="col-md-8">
							<input class="form-control" type="password">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3 control-label"></label>
						  <div class="col-md-8">
							<input class="btn btn-primary" value="Save Changes" type="button">
							<span></span>
							<input class="btn btn-default" value="Cancel" type="reset">
						  </div>
						</div>
					  </form>
					</div>
				  </div>
				</div>
		  	</div><!-- /row -->
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->
      <!--main content end-->
  </section>
