<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		
		if($this->form_validation->run() == FALSE){
			$this->load->view('auth/index');
		}else{
			redirect('login');
		}
	}
	
	public function login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$user = $this->Crud->logindetails($username);
		
		
		if($user != NULL){
			$uname = $user->username;
			$id = $user->u_id;
			$hash = $user->password;
			$userlevel = $user->userlevel;
			$status = $user->status;
			
			if($username == $uname && password_verify($password, $hash))
			{
				$sessiondata = array(
					'username' => $username,
					'u_id' => $id,
					'userlevel' => $userlevel,
					'loginuser' => TRUE
				);
				
				$this->session->set_userdata('logged_in', $sessiondata);
				
				if($status == 'active'){
					if($userlevel == 'admin'){
						$url = base_url("manager/");
					}else if($userlevel == 'manager'){
						$url = base_url("manager/");
					}else if($userlevel == 'supervisor'){
						$url = base_url("supervisor/");
					}else{
						$url = base_url("cashier/");
					}
				}else{
					$this->session->set_flashdata('disable', 'Disable!');
					redirect('logout');	
				}
				header("Location: $url");
			}else{
				$this->session->set_flashdata('error', 'Username and password did\'nt match!');
				redirect('./');
			}
		}else{
			$this->session->set_flashdata('error', 'Username and password did\'nt match!');
			redirect('./');			
		}
	}
	
	public function logout()
	{
		$this->session->unset_userdata('sessiondata');
        session_destroy();
        redirect('./', 'refresh');
	}
	public function credits()
	{
		$this->load->view('auth/credits');
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		
		if($this->form_validation->run() == FALSE){
			$this->load->view('auth/index');
		}else{
			redirect('login');
		}
	}
	
	public function login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$user = $this->Crud->logindetails($username);
		
		
		if($user != NULL){
			$uname = $user->username;
			$id = $user->u_id;
			$hash = $user->password;
			$userlevel = $user->userlevel;
			$status = $user->status;
			
			if($username == $uname && password_verify($password, $hash))
			{
				$sessiondata = array(
					'username' => $username,
					'u_id' => $id,
					'userlevel' => $userlevel,
					'loginuser' => TRUE
				);
				
				$this->session->set_userdata('logged_in', $sessiondata);
				
				if($status == 'active'){
					if($userlevel == 'admin'){
						$url = base_url("manager/");
					}else if($userlevel == 'manager'){
						$url = base_url("manager/");
					}else if($userlevel == 'supervisor'){
						$url = base_url("supervisor/");
					}else{
						$url = base_url("cashier/");
					}
				}else{
					$this->session->set_flashdata('disable', 'Disable!');
					redirect('logout');	
				}
				header("Location: $url");
			}else{
				$this->session->set_flashdata('error', 'Username and password did\'nt match!');
				redirect('./');
			}
		}else{
			$this->session->set_flashdata('error', 'Username and password did\'nt match!');
			redirect('./');			
		}
	}
	
	public function logout()
	{
		$this->session->unset_userdata('sessiondata');
        session_destroy();
        redirect('./', 'refresh');
	}
	public function credits()
	{
		$this->load->view('auth/credits');
	}
}
