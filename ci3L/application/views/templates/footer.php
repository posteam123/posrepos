    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('assets/js/jquery.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-1.8.3.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url('assets/js/jquery.dcjqaccordion.2.7.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.scrollTo.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.nicescroll.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/jquery.sparkline.js'); ?>"></script>


    <!--common script for all pages-->
    <script src="<?php echo base_url('assets/js/common-scripts.js'); ?>"></script>
    
    <script type="text/javascript" src="<?php echo base_url('assets/js/gritter/js/jquery.gritter.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/gritter-conf.js'); ?>"></script>

	<!-- Validator -->
	<script src="<?php echo base_url('assets/js/validator.js'); ?>"></script>
    <!--script for this page-->
    <script src="<?php echo base_url('assets/js/sparkline-chart.js'); ?>"></script>    
	<script src="<?php echo base_url('assets/js/zabuto_calendar.js'); ?>"></script>	
	
	<script src="<?php echo base_url('assets/js/dataTables/jquery.dataTables.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/dataTables/dataTables.bootstrap.js'); ?>"></script>
	
	<!-- No reload-->
	<script src="<?php echo base_url('assets/libs/manager.js'); ?>"></script>
	
	<!-- Scripts-->
	<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip(); 
	});
	</script>
	<script>
        function getTime()
        {
            var today=new Date();
            var h=today.getHours();
            var m=today.getMinutes();
            var s=today.getSeconds();
            // add a zero in front of numbers<10
            m=checkTime(m);
            s=checkTime(s);
            document.getElementById('showtime').innerHTML=h+":"+m+":"+s;
            t=setTimeout(function(){getTime()},500);
        }

        function checkTime(i)
        {
            if (i<10)
            {
                i="0" + i;
            }
            return i;
        }
    </script>
	
	<script>
		$(document).ready(function () {
			$('#dataTables-example').dataTable();
		});
	</script>
	
	
	
  </body>
</html>
