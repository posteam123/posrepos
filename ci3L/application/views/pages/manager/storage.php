      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
		<section class="wrapper">
		<br />
			<!-- Trigger the modal with a button -->
			<?php  if ($this->session->flashdata('error')) : ?>
				<div class="alert alert-success alert-dismissable">
					<a class="panel-close close" data-dismiss="alert">×</a> 
					<i class="fa fa-coffee"></i>
				<?php echo $this->session->flashdata('error');?>
				  </div>
			 <?php endif; ?>
			 <?php  if ($this->session->flashdata('success')) : ?>
				<div class="alert alert-success alert-dismissable">
					<a class="panel-close close" data-dismiss="alert">×</a> 
					<i class="fa fa-coffee"></i>
				<?php echo $this->session->flashdata('success');?>
				  </div>
			 <?php endif; ?>
			 
			 <?php if($this->session->flashdata('status')):?>
				<div class="alert alert-warning col-md-12" role="alert">
				<center>
					You are about to change the status of a certain user. Proceed?
					<form action="<?php echo base_url('change_empstatus');?>" method="post">
						<input type="hidden" name="id" value="<?php echo $this->session->flashdata ('statusid') ?>"/>
						<input type="hidden" name="status" value="<?php echo $this->session->flashdata('status')?>"/>
						<button type="submit" class="btn btn-warning btn-sm">Yes</button>
						<a href="" class="btn btn-info btn-sm"> No </a>
					</form>
				</center>
				</div>
				<?php endif;
				if ($this->session->flashdata('updated')) :?>
				<div class="col-md-12 alert alert-success" role="alert">
					<a href=""><span class="glyphicon glyphicon-remove pull-right"></span></a>
					&nbsp;&nbsp; The record has been successfully updated.
					<span class="glyphicon glyphicon-okay pull-left"></span>
				</div>
				<?php endif;?>
			

			<br><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal-prodTransfer"  data-backdrop="static" tabindex="-1" aria-labelledby="modal-registerLabel" aria-hidden="true" ><i class="fa fa-barcode fa-fw"></i>&nbsp;Product Transfer</button>
			
			<!--MODALS-->
			<div class="modal fade" id="modal-prodTransfer" role="dialog">
				
				
				<div class="alert alert-danger hidden" id="msg" role="alert">
				  <span class="glyphicon glyphicon-check">
				  </span>
				</div>
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Product Details</h4>
							</div>
							<div class="modal-body">
								<form id="registerForm" data-toggle="validator" method="POST">
									<div class="form-group has-feedback">
										<label for="bcode">Barcode:</label>
										<input type="text" class="form-control" id="code" name="code" data-error="Not a valid input" pattern="([0-9])*" required>
										
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										<p class="help-block with-errors"></p>
									</div>
									
									<div class="form-group has-feedback">
										<label for="typ">Type:</label>
										<input type="text" class="form-control" id="typ" name="type" pattern="([a-z]*[A-Z]*)*" data-error="Not a valid input" required>
										
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										<p class="help-block with-errors"></p>
									</div>
									
									<div class="form-group has-feedback">
										<label for="brnd">Brand:</label>
										<input type="text" class="form-control" id="brnd" name="brand" pattern="([a-z]*[A-Z]*[ ]*)*" data-error="Not a valid input" required>
										
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										<p class="help-block with-errors"></p>
									</div>
									
					

									<div class="form-group has-feedback">
										<label for="sel1">Category:</label>
											<select class="form-control" id="categ" name="category">
										<?php foreach ($category as $c) :?>
												<option>
												<?php echo $c->name;?></option>
										<?php endforeach;?>
											</select>
									</div>

									
									<div class="form-group has-feedback">
										<label for="qty">Quantity:</label>
										<input type="text" class="form-control" id="qty" name="quantity" data-error="Not a valid input" pattern="([0-9])*" required>
										
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									  <p class="help-block with-errors"></p>
									</div>
									
									<div class="form-group has-feedback">
										<label for="price">Price:</label>
										<input type="text" class="form-control" id="price" name="price" data-error="Not a valid input" pattern="([0-9]*[.]*)*" required>
										
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									  <p class="help-block with-errors"></p>
									</div>
									
									<div class="form-group has-feedback">
										<label for="damage">Damage:</label>
										<input type="text" class="form-control" id="damage" name="damage" data-error="Not a valid input" pattern="([0-9])*" required>
										
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										
									</div>
									
																	</form>
							</div>
							<div class="modal-footer">
								<button onClick="submitProd();" class="btn btn-default">Submit</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
			</div>
		  <!--END MODALS-->
			<div class="row mt">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading" style="background: #1e90ff; color:white;">
						  <h4><strong>List of Employees</strong></h4>
						</div>
						<div class="panel-body here">
							<div class="dataTable_wrapper">
								<table class="table table-bordered table-striped table-condensed" id="dataTables-example">
									<thead>
									<tr>
										<th>Employee ID</th>
										<th>Username</th>
										<th>First Name</th>
										<th>Middle Name</th>
										<th>Last Name</th>
										<th>Address</th>
										<th>Contact No.</th>
										<th>Position</th>
										<th>Actions</th>
									</tr>
									</thead>
									<tbody>
											<?php foreach ($users as $u) :$status=$u->status;?>
										<tr>
											<td><?php echo $u->id; ?></td>
											<td><?php echo $u->username; ?></td>
											<td><?php echo ucwords($u->firstname); ?></td>
											<td><?php echo ucwords($u->middlename); ?></td>
											<td><?php echo ucwords($u->lastname); ?></td>
											<td><?php echo ucwords($u->address); ?></td>
											<td><?php echo $u->contactno; ?></td>
											<td><?php echo $u->userlevel; ?></td>
											<td align="center">
											<?php if($status == 'active'):?>
												<a href="<?php echo base_url('pages/empstatus/disabled/'.$u->u_id); ?>" data-toggle="tooltip" data-placement="top"  title="Block user"><i class="fa fa-ban" aria-hidden="true"></i></a> &nbsp;
												<?php else:?>
												
												<a href="<?php echo base_url('pages/empstatus/active/'.$u->u_id); ?>" data-toggle="tooltip" data-placement="top"  title="Allow User"><i class="fa fa-check" aria-hidden="true"></i></a> &nbsp;
												
												<?php endif;?>
												
												<a href="" data-toggle="tooltip" data-placement="top"  title="Delete user"><i class="fa fa-times" aria-hidden="true"></i></a>
											</td>
										</tr>
											<?php endforeach;?>
									</tbody>
							  </table>
							</div>
						</div><!-- /content-panel -->
					</div><!-- /col-lg-4 -->			
				</div>
		  	</div><!-- /row -->
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->
      <!--main content end-->
  </section>
