      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
		<section class="wrapper">
			<div class="row mt">
				<div class="col-lg-12">
				  <h1 class="page-header">Company Settings</h1>
				  <div class="row">
					<!-- left column -->
					<div class="col-md-4 col-sm-6 col-xs-12">
					  <div class="text-center">
						<img src="<?php echo base_url('assets/img/logo.png'); ?>" class="avatar-image img-circle img-thumbnail" alt="avatar">
						<h6>Upload a different photo...</h6>
						<input type="file" class="text-center center-block well well-sm">
					  </div>
					</div>
					<!-- edit form column -->
					<div class="col-md-8 col-sm-6 col-xs-12 personal-info">
					  <div class="alert alert-info alert-dismissable">
						<a class="panel-close close" data-dismiss="alert">×</a> 
						<i class="fa fa-coffee"></i>
						This is an <strong>.alert</strong>. Use this to show important messages to the user.
					  </div>
					  <form class="form-horizontal" role="form">
						<div class="form-group">
						  <label class="col-lg-3 control-label">Company Name:</label>
						  <div class="col-lg-8">
							<input class="form-control" type="text">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-lg-3 control-label">Description:</label>
						  <div class="col-lg-8">
							<input class="form-control" type="text">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-lg-3 control-label">Address:</label>
						  <div class="col-lg-8">
							<input class="form-control" type="text">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-lg-3 control-label">Contact Number:</label>
						  <div class="col-lg-8">
							<input class="form-control" type="text">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3 control-label"></label>
						  <div class="col-md-8">
							<input class="btn btn-primary" value="Save Changes" type="button">
							<span></span>
							<input class="btn btn-default" value="Cancel" type="reset">
						  </div>
						</div>
					  </form>
					</div>
				  </div>
				</div>
		  	</div><!-- /row -->
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->
  </section>

	<script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
