      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
		  <!-- Trigger the modal with a button -->
		  <br><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"><i class="fa fa-truck fa-fw"></i>&nbsp;Received Product</button>
			<div class="modal fade" id="myModal" role="dialog">
				<div class="modal-dialog">
				
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title"></h4><br>
						</div>
						<div class="modal-body">
							<form role="form">
								<div class="form-group">
									<label for="bcode">Barcode:</label>
									<input type="text" class="form-control" id="bcode">
								</div>
								<div class="form-group">
									<label for="sel1">Category:</label>
										<select class="form-control" id="sel1">
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
										</select>
								</div>
								<div class="form-group">
									<label for="brand">Brand:</label>
									<input type="text" class="form-control" id="brand">
								</div>
								<div class="form-group">
									<label for="desc">Description:</label>
									<input type="text" class="form-control" id="desc">
								</div>
								<div class="form-group">
									<label for="qtty">Quantity:</label>
									<input type="text" class="form-control" id="qtty">
								</div>
								<div class="form-group">
									<label for="price">Price:</label>
									<input type="text" class="form-control" id="price">
								</div>
								<div class="form-group">
									<label for="price">Supplier Name:</label>
									<input type="text" class="form-control" id="price">
								</div>
								<div class="form-group">
									<label for="price">Driver's Name:</label>
									<input type="text" class="form-control" id="price">
								</div>
								<div class="form-group">
									<label for="price">Plate No.:</label>
									<input type="text" class="form-control" id="price">
								</div>
								<div class="form-group">
									<label for="price">Received By:</label>
									<input type="text" class="form-control" id="price">
								</div>
								<div class="form-group">
									<label for="price">OR no.:</label>
									<input type="text" class="form-control" id="price">
								</div>
								<div class="form-group">
									<label for="price">Date:</label>
									<input type="text" class="form-control" id="price">
								</div>
					
							</form>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-default">Submit</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</div>
				</div>
			</div>
			<!--END MODALS-->
			<div class="row mt">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading" style="background: #1e90ff; color:white;">
								  <h4><strong>List of Transactions</strong></h4>
						</div>
						<div class="panel-body here">
							<div class="dataTable_wrapper">
								<section id="unseen">
								<table class="table table-bordered table-striped table-condensed" id="dataTables-example">
								  <thead>
								  <tr>
									  <th class="numeric">Barcode</th>
									  <th>Category</th>
									  <th>Brand</th>
									  <th>Description</th>
									  <th class="numeric">Qty.</th>
									  <th class="numeric">Price</th>
									  <th>Supplier Name</th>
									  <th>Driver's Name</th>
									  <th>Plate no.</th>
									  <th>Received By</th>
									  <th>OR no.</th>
									  <th>Date</th>
								  </tr>
								  </thead>
								  <tbody>
								  <tr>
									  <th>1234</th>
									  <th>Bath Essentials</th>
									  <th>Dove</th>
									  <th>Pack</th>
									  <th>21</th>
									  <th>40.00</th>
									  <th>Mr. Basalo</th>
									  <th>CM. Recto</th>
									  <th>143</th>
									  <th>Mai-mai</th>
									  <th>08789776</th>
									  <th>04/26/16</th>
								  </tr>
								  <tr>
									  <th>1234</th>
									  <th>Bath Essentials</th>
									  <th>Dove</th>
									  <th>Pack</th>
									  <th>21</th>
									  <th>40.00</th>
									  <th>Mr. Basalo</th>
									  <th>CM. Recto</th>
									  <th>143</th>
									  <th>Mai-mai</th>
									  <th>08789776</th>
									  <th>04/26/16</th>
								  </tr>
								  <tr>
									  <th>1234</th>
									  <th>Bath Essentials</th>
									  <th>Dove</th>
									  <th>Pack</th>
									  <th>21</th>
									  <th>40.00</th>
									  <th>Mr. Basalo</th>
									  <th>CM. Recto</th>
									  <th>143</th>
									  <th>Mai-mai</th>
									  <th>08789776</th>
									  <th>04/26/16</th>
								  </tr>
								  <tr>
									  <th>1234</th>
									  <th>Bath Essentials</th>
									  <th>Dove</th>
									  <th>Pack</th>
									  <th>21</th>
									  <th>40.00</th>
									  <th>Mr. Basalo</th>
									  <th>CM. Recto</th>
									  <th>143</th>
									  <th>Mai-mai</th>
									  <th>08789776</th>
									  <th>04/26/16</th>

								  </tr>
								  <tr>
									  <th>1234</th>
									  <th>Bath Essentials</th>
									  <th>Dove</th>
									  <th>Pack</th>
									  <th>21</th>
									  <th>40.00</th>
									  <th>Mr. Basalo</th>
									  <th>CM. Recto</th>
									  <th>143</th>
									  <th>Mai-mai</th>
									  <th>08789776</th>
									  <th>04/26/16</th>
								  </tr>
								  <tr>
									  <th>1234</th>
									  <th>Bath Essentials</th>
									  <th>Dove</th>
									  <th>Pack</th>
									  <th>21</th>
									  <th>40.00</th>
									  <th>Mr. Basalo</th>
									  <th>CM. Recto</th>
									  <th>143</th>
									  <th>Mai-mai</th>
									  <th>08789776</th>
									  <th>04/26/16</th>
								  </tr>
								  </tbody>
								</table>
							  </section>
							</div><!-- /content-panel -->
						</div><!-- /col-lg-4 -->			
					</div><!-- /col-lg-4 -->			
				</div><!-- /row -->
		  	</div><!-- /row -->
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->
      <!--main content end-->
  </section>
