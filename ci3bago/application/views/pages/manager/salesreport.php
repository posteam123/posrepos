<section id="main-content">
    <section class="wrapper">
      <div class="row mt">
        <div class="col-lg-6" style=" float: left;">
          <div class="panel panel-default">
            <div class="panel-heading" style="background: #1e90ff; color:white; padding-bottom: 4px;">
                  <h4><strong> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; M &nbsp; O &nbsp; N &nbsp; T &nbsp; H &nbsp; O &nbsp; F &nbsp;  <br> <br> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; M &nbsp; A &nbsp; Y &nbsp; &nbsp; 2 &nbsp; 0 &nbsp; 1 &nbsp; 6</strong></h4>
            </div>
            <div class="panel-body here">
              <div class="dataTable_wrapper">
                <table class="table table-bordered table-striped table-condensed" style="height: 378px;">
                  <thead>
                  <tr>
                    <th class="Month" style="color: #c9aa5f; font-size: 10px;" >Day</th>
                    <th class="numeric" style="color: #c9aa5f; font-size: 10px;">Sales Value</th>
                    <th class="numeric" style="color: #c9aa5f; font-size: 10px;">Purchases Value</th>
                    <th class="numeric" style="color: #c9aa5f; font-size: 10px;">Expenses Value</th>
                    <th class="numeric" style="color: #c9aa5f; font-size: 10px;">Profit and/or Loss</th>
                  </tr>
                  </thead>
                  <tbody style="color: gray">
                  <tr>
                    <th>Mon</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  <tr>
                    <th>Tue</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  <tr>
                    <th>Wed</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  <tr>
                    <th>Thu</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  <tr>
                    <th>Fri</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  <tr>
                    <th>Sat</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  <tr>
                    <th>Sun</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  </tbody>
                </table>
                </section>
              </div><!-- /content-panel -->
            </div><!-- /col-lg-4 -->      
          </div><!-- /col-lg-4 -->      
        </div><!-- /row -->
        </div><!-- /row -->   
  </section>
</section>
<section id="main-content">
    <section class="wrapper">
      <div class="row mt">
        <div class="col-lg-6" style="float: right; margin-top: -648px;">
          <div class="panel panel-default">
            <div class="panel-heading" style="background: #1e90ff; color:white; padding-bottom: 7px;">
                  <h4><strong><br> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Y &nbsp; E &nbsp; A &nbsp; R &nbsp; &nbsp; 2 &nbsp; 0 &nbsp; 1 &nbsp; 6 <br> <br></strong></h4>
            </div>
            <div class="panel-body here">
              <div class="dataTable_wrapper">
                <table class="table table-bordered table-striped table-condensed">
                  <thead>
                  <tr>
                    <th class="Month" style="color: #c9aa5f; font-size: 10px;" >Month</th>
                    <th class="numeric" style="color: #c9aa5f; font-size: 10px;">Sales Value</th>
                    <th class="numeric" style="color: #c9aa5f; font-size: 10px;">Purchases Value</th>
                    <th class="numeric" style="color: #c9aa5f; font-size: 10px;">Expenses Value</th>
                    <th class="numeric" style="color: #c9aa5f; font-size: 10px;">Profit and/or Loss</th>
                  </tr>
                  </thead>
                  <tbody style="color: gray">
                  <tr>
                    <th>January</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  <tr>
                    <th>February</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  <tr>
                    <th>March</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  <tr>
                    <th>April</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  <tr>
                    <th>May</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  <tr>
                    <th>June</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  <tr>
                    <th>July</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  <tr>
                    <th>August</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  <tr>
                    <th>September</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  <tr>
                    <th>October</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  <tr>
                    <th>November</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  <tr>
                    <th>December</th>
                    <th>659,890.97</th>
                    <th>4,833.00</th>
                    <th>400.00</th>
                    <th>654,657.97</th>
                  </tr>
                  </tbody>
                </table>
                </section>
              </div><!-- /content-panel -->
            </div><!-- /col-lg-4 -->      
          </div><!-- /col-lg-4 -->      
        </div><!-- /row -->
        </div><!-- /row -->   
  </section>
</section>
