// JavaScript Document
function submitdata() {
	var  $content = {};
	var id = document.getElementById("id").value;
	var uname = document.getElementById("uname").value;
	var fname = document.getElementById("fname").value;
	var mname = document.getElementById("mname").value;
	var lname = document.getElementById("lname").value;
	var address = document.getElementById("address").value;
	var contactNo = document.getElementById("contact").value;
	var userlvl = document.getElementById("userlvl").value;	
	
	var  data = {
			"username" : uname,
			"id" : id,
			"firstname" : fname,
			"middlename" : mname,
			"lastname" : lname,
			"address" : address,
			"contactno" : contactNo,
			"userlevel" : userlvl
		}
    // Returns successful data submission message when the entered information is stored in database.
        if (id == '' || uname == '' || fname == '' || mname == '' || lname == '' || address == '' || contact == '' || userlvl == '') {
			alert("Please Fill All Fields");
        }
        else{
			// AJAX code to submit form.
			$.ajax({
			type: "POST",
			url: "http://localhost/ci3/addemployee",
			data: data,
			cache: false,
			success: function(html) {
			alert("Successfully Registered");
			$('#registerForm')[0].reset(); // To reset form fields
			$("#dataTables-example").load(location.href + " #dataTables-example");
			}
			});
		}
        return false;
}

// JavaScript Document
function submitProd() {
	var  $content = {};
	var bcode = document.getElementById("code").value;
	var type = document.getElementById("typ").value;
	var brand = document.getElementById("brnd").value;
	var category = document.getElementById("categ").value;
	var quantity = document.getElementById("qty").value;
	var price = document.getElementById("price").value;
	var damage = document.getElementById("damage").value;
	
	
	var  data = {
			"barcode" : bcode,
			"type" : type,
			"brand" : brand,
			"c_id" : category,
			"qtty" : quantity,
			"price" : price,
			"damage" : damage
		}
    // Returns successful data submission message when the entered information is stored in database.
        if (code == '' || typ == '' || brnd == '' || categ == '' || qty == '' || price == '' || damage == '') {
			alert("Please Fill All Fields");
        }
        else{
			// AJAX code to submit form.
			$.ajax({
			type: "POST",
			url: "http://localhost/ci3L/prodTransfer",
			data: data,
			cache: false,
			success: function(html) {
			alert("Successfully Added");
			$('#registerForm')[0].reset(); // To reset form fields
			$("#dataTables-example").load(location.href + " #dataTables-example");
			}
			});
		}
        return false;
}
