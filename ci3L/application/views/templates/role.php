<?php
	if(!$this->session->userdata('logged_in')) {
		redirect('./');
	}else{
		$ses=$this->session->userdata('logged_in'); 
		$role_ses=$ses['userlevel'];
		if($role_ses != $role){
			redirect($role_ses.'/');
		}
	}
?>