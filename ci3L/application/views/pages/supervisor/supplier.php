      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
		  <!-- Trigger the modal with a button -->
		  <br><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus fa-fw"></i>&nbsp;Add Supplier</button>
			<div class="modal fade" id="myModal" role="dialog">
				
				
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Supplier Details</h4>
						</div>
						<div class="modal-body">
							<form id="registerForm"role="form" name="form" data-toggle="validator" method="post" action="">
				
								<div class="alert alert-danger hidden" id="msg" role="alert">
								  <span class="glyphicon glyphicon-check">
								  </span>
								</div>
								
								<div class="form-group has-feedback">
									<label for="supplier name">Supplier Name:</label>
									<input type="text" class="form-control" id="suppliername" name="suppliername" pattern="([a-z]*[A-Z]*)*" data-error="Not a valid input" required>
									
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									<p class="help-block with-errors"></p>
								</div>
								<div class="form-group has-feedback">
									<label for="contact">Contact:</label>
									<input type="text" class="form-control" id="suppliername" name="suppliername" pattern="([0-9])*" data-error="Not a valid input" required>
									
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									<p class="help-block with-errors"></p>
								</div>
								<div class="form-group has-feedback">
									<label for="address">Address:</label>
									<input type="text" class="form-control" id="address" name="address" data-error="Not a valid input" pattern="([a-z]*[A-Z]*[0-9]*[#,. ]*)*" required>
									
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								  <p class="help-block with-errors"></p>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-default">Submit</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</div>
				</div>
			</div>
			<!--END MODALS-->
			<div class="row mt">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading" style="background: #1e90ff; color:white;">
								  <h4><strong>List of Supplier</strong></h4>
						</div>
						<div class="panel-body here">
							<div class="dataTable_wrapper">
								<section id="unseen">
								<table class="table table-bordered table-striped table-condensed" id="dataTables-example">
								  <thead>
								  <tr>
									  <th>Supplier Name</th>
									  <th>Contact</th>
									  <th>Address</th>
									  <th>Actions</th>
								  </tr>
								  </thead>
								  <tbody>
								  <tr>
									  <td>Mr. Basalo</td>
									  <td>08789776</td>
									  <td>CM. Recto</td>
									  <td class="numeric" align="center">
											<a href=""><i class="fa fa-ban" aria-hidden="true"></i></a>
											<a href=""><i class="fa fa-times" aria-hidden="true"></i></a>
									  </td>
								  </tr>
								  <tr>
									  <td>Mr. Basalo</td>
									  <td>08789776</td>
									  <td>CM. Recto</td>
									  <td class="numeric" align="center">
											<a href=""><i class="fa fa-ban" aria-hidden="true"></i></a>
											<a href=""><i class="fa fa-times" aria-hidden="true"></i></a>
									  </td>
								  </tr>
								  <tr>
									  <td>Mr. Basalo</td>
									  <td>08789776</td>
									  <td>CM. Recto</td>
									  <td class="numeric" align="center">
											<a href=""><i class="fa fa-ban" aria-hidden="true"></i></a>
											<a href=""><i class="fa fa-times" aria-hidden="true"></i></a>
									  </td>
								  </tr>
								  </tbody>
								</table>
							  </section>
							</div><!-- /content-panel -->
						</div><!-- /col-lg-4 -->			
					</div><!-- /col-lg-4 -->			
				</div><!-- /row -->
		  	</div><!-- /row -->
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->
      <!--main content end-->
  </section>
